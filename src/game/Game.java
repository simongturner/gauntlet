package game;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.Timer;

public class Game extends JPanel implements ActionListener, KeyListener{
	
	private final int WIDTH = 300;
	private final int HEIGHT = 720;
	public static boolean keys[] = new boolean[256];

	Timer timer = new Timer(20, this);
	Player hero = new Player(WIDTH/2, HEIGHT/2, 100,100);
	
	public Game(){
		
		this.setPreferredSize(new Dimension(WIDTH, HEIGHT));
		this.setBackground(Color.BLACK);
		
		JFrame frame = new JFrame();
		frame.setResizable(false);
		frame.setSize(WIDTH, HEIGHT);
		frame.setContentPane(this);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		
		frame.addKeyListener(this);
		frame.setVisible(true);
		this.timer.start();
		
	}
	
	public static void main(String[] args){
		
		new Game();
		
	}
	
	private void update(){

		hero.updatePlayer();
		
	}
	
	@Override
	public void paintComponent(Graphics g){
		
		Graphics2D g2d = (Graphics2D)g;
		super.paintComponent(g2d);
		
		g2d.setColor(Color.WHITE);
		g2d.fill(hero.getPlayerDimensions());
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
		keys[e.getKeyCode()] = true;
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
		keys[e.getKeyCode()] = false;
		
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(e.getSource() == timer){
			
			update();
			repaint();
			
		}
		
		
	}
	
}
