package game;

import java.awt.Rectangle;
import java.awt.event.KeyEvent;

public class Player {

	private Rectangle playerDimensions = new Rectangle(0,0,0,0);

	public Rectangle getPlayerDimensions(){
		return playerDimensions;
	}	
	
	public Player(int x, int y, int width, int height){

		playerDimensions.x = x;
		playerDimensions.y = y;
		playerDimensions.width = width;
		playerDimensions.height = height;
		
	}
	
	public void updatePlayer(){
		
		if(Game.keys[KeyEvent.VK_A]){
			playerDimensions.x++;
		}	
		if(Game.keys[KeyEvent.VK_D]){
			playerDimensions.x--;
		}
		
	}	
	
}
